#include <iostream>
#include <stdlib.h>
#include <limits>
using namespace std;

struct nodo{
       int nro;
       char producto;
       struct nodo *sgte;
};

typedef struct nodo *Tlista;

/**Insertar al final**/
void encolar(Tlista &cola, int aux_nro, char aux_producto)
{
    Tlista nuevo = new(struct nodo);

    nuevo->nro  = aux_nro;
    nuevo->producto = aux_producto;
    nuevo->sgte = NULL;

    nuevo->sgte = cola;
    cola  = nuevo;

}

/**Insertar al final**/
void encolar(Tlista &cola,char producto, int &n)
{
    n++;
    Tlista nuevo = new(struct nodo);
    nuevo->nro=n;
    nuevo->producto = producto;
    nuevo->sgte = NULL;

    if(cola==NULL)
    {
        cola = nuevo;
    }
    else
    {
        Tlista p = cola;
        while(p->sgte!=NULL)
        {
            p = p->sgte;
        }
        p->sgte = nuevo;
    }

}

void desencolar(Tlista &cola)
{
    Tlista p=cola;
    cola=cola->sgte;
    delete(p);
}

/**Insertar al inicio**/
void push(Tlista &pila, int aux_nro, char aux_producto)
{
    Tlista nuevo;
    nuevo = new(struct nodo);
    nuevo->nro = aux_nro;
    nuevo->producto=aux_producto;
    nuevo->sgte = pila;
    pila  = nuevo;
}

/**Eliminar al inicio**/
void traslado(Tlista &cola, Tlista &pila)
{
    Tlista aux=cola;
    int aux_nro=aux->nro;
    char aux_producto=aux->producto;
    push(pila,aux_nro,aux_producto);
    cola=cola->sgte;
    delete(aux);

}

void eliminarPrimerElemento(Tlista &cola)
{

        Tlista p=cola;
        cola=cola->sgte;
        delete(p);


}

void pop(Tlista &pila, Tlista &cola)
{
    Tlista aux=pila;
    int aux_nro=aux->nro;
    char aux_producto=aux->producto;
    encolar(cola,aux_nro,aux_producto);
    pila=pila->sgte;
    delete(aux);

}

void mostrarPila(Tlista pila)
{
     while(pila != NULL)
     {
          cout <<"\t\t\t\t\t\t\t      ||  " <<pila->producto<<"/"<<pila->nro<<"   ||"<<endl;
          pila = pila->sgte;
     }
}

void mostrarCola(Tlista cola)
{
    cout<<"_______________________________"<<endl<<endl;
    while(cola!= NULL){
        cout<<cola->producto<<"/"<<cola->nro<<"  ";
        cola=cola->sgte;
    }
    cout<<endl;
    cout<<"_______________________________"<<endl;
}


void menu1()
{
    cout<<"\n\t\tLISTA ENLAZADA SIMPLE\n\n";
    cout<<" 1. INGRESAR PRODUCTOS A LA CINTA        "<<endl;
    cout<<" 2. ELIMINAR PRODUCTO (B) DEFECUTOSO   "<<endl;
    cout<<" 3. ATENDER PRODUCTO                 "<<endl;
    cout<<" 4. MOSTRAR                          "<<endl;
    cout<<" 0. SALIR                            "<<endl;

    cout<<"\n INGRESE OPCION: ";
}

/*                        Funcion Principal
---------------------------------------------------------------------*/

int main()
{
    Tlista pila = NULL, cola=NULL;
    char op;     // opcion del menu
    char producto;
    int n=0;


    system("color 0b");

    do
    {
        menu1();  cin>> op;

        switch(op)
        {
            case '1':

                cout<< "\n INGRESE PRODUCTO: "; cin>> producto;
                while(producto!='A'&& producto!='B'&& producto!='C'&& producto!='D'){
                    cout<<"\nPRODUCTO NO EXISTENTE, SOLO SE PUEDE INGRESAR 4 PRODUCTOS";
					cout<<"\nY CON LETRAS A,B,C,D COMO INDICA EL PROBLEMA...";
                    cout<< "\n INGRESE PRODUCTO: "; cin>> producto;
                }
                encolar(cola,producto,n);
                cout<<"\n<-- CINTA DE DESPLAZAMIENTO <--"<<endl;
                mostrarCola(cola);

            break;


            case '2':
                traslado(cola,pila);
                mostrarPila(pila);
                mostrarCola(cola);
                cout<<"\nProducto "<<cola->producto<<" eliminado..."<<endl;
                eliminarPrimerElemento(cola);
                mostrarPila(pila);
                mostrarCola(cola);
                cout<<"\nDevolvemos Producto A a la cola..."<<endl;
                pop(pila,cola);
                mostrarCola(cola);

            break;


            case '3':
                if(cola==NULL){
                    cout<<endl;
                    cout<<"||No se puede realizar la operacion||"<<endl;
                    cout<<"||     Cola de Productos vacia     ||"<<endl;
                    cout<<"\n<-- CINTA DE DESPLAZAMIENTO <--"<<endl;
                    mostrarCola(cola);
                }
                else{
                    cout<<"\nProducto "<<cola->producto<<" atendido..."<<endl;
                    desencolar(cola);
                    cout<<"\n<-- CINTA DE DESPLAZAMIENTO <--"<<endl;
                    mostrarCola(cola);

                }

            break;


            case '4':

                cout<<"\n<-- CINTA DE DESPLAZAMIENTO <--"<<endl;
                mostrarCola(cola);

            break;

            case '0':

            break;

            default:

                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(),'\n');
                cout<<"\nOPCION INCORRECTA!";


        }

        cout<<endl<<endl;
        system("pause");  system("cls");

    }while(op!='0');


   system("pause");
   return 0;
}
