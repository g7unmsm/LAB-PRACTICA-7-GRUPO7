#include <iostream>
#include <stdlib.h>
using namespace std;

struct nodoDoble{
       int nro;        // en este caso es un numero entero
       struct nodoDoble *sgte,*ant;
}; 
typedef struct nodoDoble *TlistaDoble;

struct nodoSimple{
       int nro;        // en este caso es un numero entero
       struct nodoSimple *sgte;
}; 
typedef struct nodoSimple *TlistaSimple;

void insertarFinal(TlistaDoble &lista, int valor){
    TlistaDoble nuevo=NULL;
    nuevo=new(struct nodoDoble);
    nuevo->nro=valor;
    nuevo->sgte=NULL;
    nuevo->ant=NULL;

    if(lista==NULL){
        lista=nuevo;
    }
    else{
        TlistaDoble p=lista;
        while(p->sgte!=NULL){
            p=p->sgte;
        }

        p->sgte=nuevo;
        nuevo->ant=p;
        nuevo->sgte=NULL;
    }
}

void insertarCircularSimple(TlistaSimple &lista, int valor){
    TlistaSimple nuevo = new(struct nodoSimple);

    nuevo->nro  = valor;
    nuevo->sgte = NULL;

    if(lista==NULL)
    {
        lista = nuevo;
        nuevo->sgte=lista;
    }
    else
    {
        TlistaSimple p = lista;
        while(p->sgte!=lista)
        {
            p = p->sgte;
        }
        p->sgte = nuevo;
        nuevo->sgte=lista;
    }
}

void insertarEnlazadaSimple(TlistaSimple &lista, int valor){
    TlistaSimple t, q = new(struct nodoSimple);

    q->nro  = valor;
    q->sgte = NULL;

    if(lista==NULL)
    {
        lista = q;
    }
    else
    {
        t = lista;
        while(t->sgte!=NULL)
        {
            t = t->sgte;
        }
        t->sgte = q;
    }

}

int media(TlistaDoble lista){
	int suma=0, n=0;
	while(lista!=NULL){
		suma = suma + lista->nro;
		n=n+1;
		lista=lista->sgte;
	}
	int prom=suma/n;
	return prom;
}

void crearListas(TlistaDoble lista, int prom, TlistaSimple &listaCircular, TlistaSimple &listaSimple){
	while(lista != NULL){
		if(lista->nro <= prom){
			insertarCircularSimple(listaCircular,lista->nro);
		}else{
			insertarEnlazadaSimple(listaSimple,lista->nro);
		}
		lista=lista->sgte;
	}
}

void mostrarLista1(TlistaDoble lista){
     if(lista==NULL){
        cout<<"\n\tLista vacia!"<<endl;
     }
     else
     {
        int n = 0;
        TlistaDoble p=NULL;
        while(lista!= NULL)
        {
          cout <<' '<< n+1 <<") " << lista->nro <<" ";
          p=lista;
          lista = lista->sgte;
          n++;
        }
     }
}

void mostrarLista2(TlistaSimple lista)
{
     if(lista==NULL){
        cout<<"\n\tLista vacia!"<<endl;
     }
     else
     {
         TlistaSimple p=lista;
        int n = 0;
        cout <<' '<< n+1 <<") " << p->nro<<" ";
        while(p->sgte != lista)
        {
            p = p->sgte;
            n++;
            cout <<' '<< n+1 <<") " << p->nro<<" ";
        }
     }

}

void mostrarLista3(TlistaSimple lista){
     if(lista==NULL){
        cout<<"\n\tLista vacia!"<<endl;
     }
     else
     {
        int n = 0;
        TlistaSimple p=NULL;
        while(lista!= NULL)
        {
          cout <<' '<< n+1 <<") " << lista->nro <<" ";
          p=lista;
          lista = lista->sgte;
          n++;
        }
     }
}

void menu1(){
    cout<<"\n\t\tPROB-2 LISTAS\n\n";
    cout<<" 1. INSERTAR ELEMENTO              "<<endl;
    cout<<" 2. CREAR LISTAS           "<<endl;
	cout<<" 3. MOSTRAR LISTAS           "<<endl;
	cout<<" 0. SALIR           "<<endl;
    cout<<"\n INGRESE OPCION: ";
}

int main()
{
    TlistaDoble lista = NULL;
    TlistaSimple listaCircular = NULL;
    TlistaSimple listaSimple = NULL;
    int op;     // opcion del menu
    int _dato;  // elemenento a ingresar
    int pos, prom;    // posicion a insertar


    system("color 0b");

    do
    {
        menu1();  cin>> op;

        switch(op)
        {
            case 1:
                 cout<< "\n NUMERO A INSERTAR: "; cin>> _dato;
                 insertarFinal(lista, _dato);
                 mostrarLista1(lista);
            break;
            
            case 2:
            	prom = media(lista);
            	crearListas(lista, prom, listaCircular, listaSimple);
            	cout<<" Tarea completada...";
            break;
            
            case 3:
            	cout<<endl;
            	mostrarLista1(lista);cout<<endl;
            	mostrarLista2(listaCircular);cout<<endl;
            	mostrarLista3(listaSimple);cout<<endl;
            break;
    	}

        cout<<endl<<endl;
        system("pause");  system("cls");

    }while(op!=0);


   system("pause");
   return 0;
}
