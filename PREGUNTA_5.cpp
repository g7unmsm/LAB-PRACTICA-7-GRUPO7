#include <iostream>
#include <stdlib.h>
#include <limits>
using namespace std;

struct nodo{
       char dato;        // en este caso es un numero entero
       struct nodo *sgte,*ant;
};
typedef struct nodo *Tlista;

void insertarInicio(Tlista &lista, int valor)
{
    Tlista nuevo=NULL;
    nuevo=new(struct nodo);
    nuevo->dato=valor;
    nuevo->sgte=NULL;
    nuevo->ant=NULL;

    if(lista==NULL){
        lista=nuevo;
    }
    else{
        nuevo->sgte=lista;
        lista->ant=nuevo;
        lista=nuevo;
        lista->ant=NULL;
    }


}

int posicionUltimoElemento(Tlista lista)
{
    int n=0;
    while(lista!=NULL){
        n++;
        lista=lista->sgte;
    }
    return n;
}

void insertarFinal(Tlista &lista, char valor)
{
    Tlista nuevo=NULL;
    nuevo=new(struct nodo);
    nuevo->dato=valor;
    nuevo->sgte=NULL;
    nuevo->ant=NULL;

    if(lista==NULL){
        lista=nuevo;
    }
    else{
        Tlista p=lista;
        while(p->sgte!=NULL){
            p=p->sgte;
        }

        p->sgte=nuevo;
        nuevo->ant=p;
        nuevo->sgte=NULL;
    }


}

void insertarElemento(Tlista &lista, char valor, int pos)
{
    if(pos==1){
        insertarInicio(lista,valor);
    }
    else{
        if(pos==posicionUltimoElemento(lista)+1){
            insertarFinal(lista, valor);
        }
        else{
            Tlista nuevo=NULL;
            nuevo=new(struct nodo);
            nuevo->dato=valor;
            nuevo->sgte=NULL;
            nuevo->ant=NULL;

            Tlista p=lista;
            int n=1;

            while(n!=pos){
                n++;
                p=p->sgte;
            }

            nuevo->sgte=p;
            nuevo->ant=p->ant;
            p->ant=nuevo;
            nuevo->ant->sgte=nuevo;
        }

    }


}

void insertarEntre(Tlista &lista, char valor){
	Tlista p=lista;
	int n=0;
	bool error=false;
	char dato1, dato2;
	cout<<"\tIngrese primer nodo: ";cin>>dato1;
	cout<<"\n\tIngrese segundo nodo: ";cin>>dato2;
	while(p->sgte!=NULL){
		n=n+1;
		if(p->dato==dato1 && p->sgte->dato==dato2){
			insertarElemento(lista,valor,n+1);
			error=true;
		}
		p=p->sgte;
	}
	if(error){
		cout<<"Error en la insercion"<<endl;
	}
}

void mostrarLista1(Tlista lista)
{
     if(lista==NULL){
        cout<<"\n\n\tLista vacia!\n"<<endl;
     }
     else
     {
        while(lista!= NULL)
        {
          cout <<lista->dato <<"  ";
          lista = lista->sgte;
        }
     }
}

void mostrarLista2(Tlista lista)
{
     if(lista==NULL){
        cout<<"\n\n\tLista vacia!\n"<<endl;
     }
     else
     {
     	Tlista p = lista;
        while(lista->sgte!= NULL)
        {
          lista = lista->sgte;
        }
        while(lista!=NULL){
        	cout <<lista->dato <<"  ";
          	lista = lista->ant;
		}
     }
}

void menu1()
{
    cout<<"\n\t\tLISTA ENLAZADA DOBLE\n\n";
    cout<<" 1. INSERTAR             "<<endl;
    cout<<" 2. INSERTAR EN UNA POSICION         "<<endl;
    cout<<" 3. INSERTAR ENTRE:         "<<endl;
    cout<<" 4. MOSTRAR LISTA (-->)                    "<<endl;
    cout<<" 5. MOSTRAR LISTA (<--)                    "<<endl;
    cout<<" 0. SALIR                            "<<endl;

    cout<<"\n INGRESE OPCION: ";
}

/*                        Funcion Principal
---------------------------------------------------------------------*/

int main()
{
    Tlista lista = NULL;
    char op;     // opcion del menu
    char _dato;  // elemenento a ingresar
    int pos;    // posicion a insertar


    system("color 0b");

    do
    {
        menu1();  cin>> op;

        switch(op)
        {
            case '1':
                 cout<< "\n DATO A INSERTAR: "; cin>> _dato;
                 insertarFinal(lista, _dato);
                 cout<<endl;
                 mostrarLista1(lista);
                 cout<<endl;
            break;

            case '2':
            	cout<< "\n DATO A INSERTAR: ";cin>> _dato;
                cout<< " Posicion : ";       cin>> pos;
                while(pos<=0 || pos>posicionUltimoElemento(lista)+1){
                    cout<<"\n\t-->LA POSICION NO EXISTE!, ingrese una posicion correcta..."<<endl;
                	cout<< " Posicion : ";       cin>> pos;
                }
                insertarElemento(lista, _dato, pos);
                cout<<endl;
                mostrarLista1(lista);
                cout<<endl;
            break;

            case '3':
            	cout<< "\n DATO A INSERTAR: ";cin>> _dato;
            	insertarEntre(lista,_dato);
            	cout<<endl;
            	mostrarLista1(lista);
                cout<<endl;
            break;


            case '4':
                cout<<endl;
            	mostrarLista1(lista);
            	cout<<endl;
            break;

            case '5':
                cout<<endl;
            	mostrarLista2(lista);
            	cout<<endl;
            break;
            case '0':

            break;

            default:

                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(),'\n');
                cout<<"\nOPCION INCORRECTA!";

        }

        cout<<endl<<endl;
        system("pause");  system("cls");

    }while(op!='0');


   system("pause");
   return 0;
}
