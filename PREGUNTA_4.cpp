#include <iostream>
#include <stdlib.h>
using namespace std;

struct nodo{
       char nro;        // en este caso es un numero entero
       struct nodo *sgte,*ant;
};

typedef struct nodo *Tlista;

int posicionUltimoElemento(Tlista lista)
{
    int n=0;
    while(lista!=NULL){
        n++;
        lista=lista->sgte;
    }
    return n;
}

void insertarInicio(Tlista &lista, char valor)
{
    Tlista nuevo=NULL;
    nuevo=new(struct nodo);
    nuevo->nro=valor;
    nuevo->sgte=NULL;
    nuevo->ant=NULL;

    if(lista==NULL){
        lista=nuevo;
    }
    else{
        nuevo->sgte=lista;
        lista->ant=nuevo;
        lista=nuevo;
        lista->ant=NULL;
    }


}

void insertarFinal(Tlista &lista, char valor)
{
    Tlista nuevo=NULL;
    nuevo=new(struct nodo);
    nuevo->nro=valor;
    nuevo->sgte=NULL;
    nuevo->ant=NULL;

    if(lista==NULL){
        lista=nuevo;
    }
    else{
        Tlista p=lista;
        while(p->sgte!=NULL){
            p=p->sgte;
        }

        p->sgte=nuevo;
        nuevo->ant=p;
        nuevo->sgte=NULL;
    }


}

void insertarElemento(Tlista &lista, char valor, int pos)
{

    if(pos==1){
        insertarInicio(lista,valor);
    }
    else{
        if(pos==posicionUltimoElemento(lista)+1){
            insertarFinal(lista, valor);
        }
        else{
            Tlista nuevo=NULL;
            nuevo=new(struct nodo);
            nuevo->nro=valor;
            nuevo->sgte=NULL;
            nuevo->ant=NULL;

            Tlista p=lista;
            int n=1;

            while(n!=pos){
                n++;
                p=p->sgte;
            }

            nuevo->sgte=p;
            nuevo->ant=p->ant;
            p->ant=nuevo;
            nuevo->ant->sgte=nuevo;
        }

    }


}

void eliminarPrimerElemento(Tlista &lista)
{
    Tlista p=lista;
    if(lista->sgte!=NULL){
        lista=lista->sgte;
        lista->ant=NULL;
    }else{
        lista=NULL;
    }
    delete(p);
}




void mostrarLista(Tlista lista)
{
     if(lista==NULL){
        cout<<"\n\tLista vacia!"<<endl;
     }
     else
     {
         int n = 0;
        Tlista p=NULL;
        while(lista!= NULL)
        {
          cout <<' '<< n+1 <<") " << lista->nro <<" ";
          p=lista;
          lista = lista->sgte;
          n++;
        }
     }

}

void mostrarListaReves(Tlista lista)
{
     if(lista==NULL){
        cout<<"\n\tLista vacia!"<<endl;
     }
     else
     {
         int n = 0;
        Tlista p=NULL;
        while(lista->sgte!= NULL)
        {
          p=lista;
          lista = lista->sgte;
        }
        while(lista!= NULL)
        {
          cout <<' '<< n+1 <<") " << lista->nro <<" ";
          p=lista;
          lista = lista->ant;
          n++;
        }
     }

}

void menu1()
{
    cout<<"\n\t\tLISTA ENLAZADA DOBLE\n\n";
    cout<<" 1. INSERTAR AL INICIO               "<<endl;
    cout<<" 2. INSERTAR AL FINAL                "<<endl;
    cout<<" 3. CONVERTIR A RAMON          "<<endl;
    cout<<" 4. MOSTRAR LISTA CONVERTIDA     "<<endl;
    cout<<" 5. MOSTRAR LISTA                    "<<endl;
    cout<<" 0. SALIR                            "<<endl;

    cout<<"\n INGRESE OPCION: ";
}

/*                        Funcion Principal
---------------------------------------------------------------------*/

int main()
{
    Tlista lista = NULL;
    int op;     // opcion del menu
    char _dato;  // elemenento a ingresar
    int pos;    // posicion a insertar
    char aux;

    system("color 0b");

    do
    {
        menu1();  cin>> op;

        switch(op)
        {
            case 1:

                 cout<< "\n LETRA A INSERTAR: "; cin>> _dato;
                 insertarInicio(lista, _dato);
                 cout<<endl;
                 mostrarLista(lista);

            break;


            case 2:

                 cout<< "\n LETRA A INSERTAR: "; cin>> _dato;
                 insertarFinal(lista, _dato );
                 cout<<endl;
                 mostrarLista(lista);


            break;


            case 3:

                aux=lista->nro;
                eliminarPrimerElemento(lista);
                insertarElemento(lista,aux,3);
                cout<<endl;
                cout<< "\n CONVIRTIENDO.... ";
                cout<<endl;

            break;

            case 4:
				cout<<endl;
            mostrarListaReves(lista);
            cout<<endl;
            break;


            case 5:

                cout << "\n\n MOSTRANDO LISTA\n\n";
                cout<<endl;
                mostrarLista(lista);
                cout<<endl;

            break;

                    }

        cout<<endl<<endl;
        system("pause");  system("cls");

    }while(op!=0);


   system("pause");
   return 0;
}
