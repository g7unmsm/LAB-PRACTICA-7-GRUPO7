#include <iostream>
#include <stdlib.h>
using namespace std;

int n;

void llenarVector(int lista[],int n)
{
    for(int i=0;i<n;i++){
        lista[i]=rand()%100+1;
    }
}

void mostrarVector(int lista[],int n)
{
    for(int i=0;i<n;i++){
        cout<<lista[i]<<" ";
    }
}

void quicksort(int lista[], int primero, int ultimo)
{
    int i,j,central;
    int pivote;

    central=(primero+ultimo)/2;
    pivote=lista[central];

    i=primero;
    j=ultimo;

    do
    {
        while(lista[i]<pivote) i++;
        while(lista[j]>pivote) j--;

        if(i<=j)
        {
            int aux;
            aux=lista[i];
            lista[i]=lista[j];
            lista[j]=aux;
            i++;
            j--;
        }

    }while(i<=j);

    mostrarVector(lista,n);
    cout<<"\n\n\t";
    if(primero<j)
        quicksort(lista,primero,j);
    if(i<ultimo)
        quicksort(lista,i,ultimo);
}

void menu1()
{
    cout<<"\n\t\tLISTA ENLAZADA SIMPLE\n\n";
    cout<<" 1. TAMANIO DE LA LISTA               "<<endl;
    cout<<" 2. ORDENAR POR QUICKSORT PASO A PASO"<<endl;
    cout<<" 0. SALIR                            "<<endl;

    cout<<"\n INGRESE OPCION: ";
}



int main()
{

    int lista[100];
    int op;

    system("color 0b");

    do
    {
        menu1();  cin>> op;

        switch(op)
        {
            case 1:

                 cout<< "\n\n INGRESE LA CANTIDAD DE ELEMENTOS : "; cin>> n;
                 llenarVector(lista,n);
                 cout<<"\n\t";
                 mostrarVector(lista,n);

            break;


            case 2:
                cout<<"\nORDENANDO POR QUICKSORT\n\n\t";
                mostrarVector(lista,n);cout<<"\n\n\t";
                quicksort(lista,0,n-1);
                mostrarVector(lista,n);

            break;

                    }

        cout<<endl<<endl;
        system("pause");  system("cls");

    }while(op!=0);


   system("pause");
   return 0;
}

